<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Post;

class PostController extends Controller{

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['user', 'manager', 'admin']
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $model = Post::find()->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}